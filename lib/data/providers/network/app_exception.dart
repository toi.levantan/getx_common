// catch error ex
class AppException implements Exception {
  final code;
  final message;
  final details;

  AppException({this.code, this.message, this.details});

// return result String code and message and details
  String toString() {
    return '$code$message$details';
  }
}

class FetchDataException extends AppException {
  FetchDataException(String? details)
      : super(
            code: 'fetch-data',
            message: 'Có lỗi trong quá trình kết nối',
            details: details);
}

class BadRequestException extends AppException {
  BadRequestException(String? details)
      : super(
            code: "invalid-request",
            message: "Có lỗi xảy ra",
            details: details);
}

class UnauthorisedException extends AppException {
  UnauthorisedException(String? details)
      : super(
            code: "unauthorised",
            message: "Chưa được cấp phép",
            details: details);
}

class InvalidInputException extends AppException {
  InvalidInputException(String? details)
      : super(
          code: "invalid-input",
          message: "Thông tin đầu vào không hợp lệ",
          details: details,
        );
}
class AuthenticationException extends AppException {
  AuthenticationException(String? details)
      : super(
    code: "authentication-failed",
    message: "Xác thực thất bại",
    details: details,
  );
}
class TimeOutException extends AppException {
  TimeOutException(String? details)
      : super(
    code: "request-timeout",
    message: "Hết thời gian yêu cầu",
    details: details,
  );
}

