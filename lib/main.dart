import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:getx_common/app/config/app_constants/language_path/language_path.dart';
import 'package:getx_common/app/services/location_services.dart';
import 'package:getx_common/app/utils/utils.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await PreferenceUtils.init();
  runApp(MyApp());
}

ThemeData _darkTheme = ThemeData(
    brightness: Brightness.dark,
    primaryColor: Colors.amber,
    buttonTheme: const ButtonThemeData(
      buttonColor: Colors.amber,
      disabledColor: Colors.grey,
    ));
List<ThemeData> listTheme = [
 ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.blue,
    buttonTheme: const ButtonThemeData(
      buttonColor: Colors.blue,
      disabledColor: Colors.grey,
    )),
  ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.red,
    appBarTheme: const AppBarTheme(color: Colors.green) ,
    buttonTheme:  const ButtonThemeData(
      buttonColor: Colors.green,
      disabledColor: Colors.green,
    )),
];

class MyApp extends StatefulWidget {

   MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}
class _MyAppState extends State<MyApp> {
  final RxBool _isLightTheme = false.obs;
  final RxBool _isEN = false.obs;
  _saveThemeStatus() async {
    await PreferenceUtils.setBool(key: 'theme', value: _isLightTheme.value);
  }

  _getThemeStatus() async {
    print("bbb ${ PreferenceUtils.getBool(key: "theme")}");
    var isLight =  PreferenceUtils.getBool(key: "theme").obs;
    _isLightTheme.value =  isLight.value;
    Get.changeThemeMode(_isLightTheme.value ? ThemeMode.light : ThemeMode.dark);
  }
  @override
  void initState() {
    // TODO: implement initState
    _getThemeStatus();

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: listTheme[1],
      darkTheme: _darkTheme,
      themeMode: ThemeMode.system,
      debugShowCheckedModeBanner: false,
      locale: LocalizationService.locale,
      fallbackLocale: LocalizationService.fallbackLocale,
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      translations: LocalizationService(),
      home: Scaffold(
        appBar: AppBar(
          title:  Text('Dark Mode Demo ${hello.tr}'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Obx(
                    () => Text(
                  'Click on switch to change to ${_isLightTheme.value ? 'Dark' : 'Light'} theme',
                ),
              ),
              ObxValue(
                    (data) => Switch(
                  value: _isLightTheme.value,
                  onChanged: (val) {
                    _isLightTheme.value = val;
                    Get.changeThemeMode(
                      _isLightTheme.value ? ThemeMode.light : ThemeMode.dark,
                    );
                    _saveThemeStatus();
                  },
                ),
                false.obs,
              ),
              Obx(
                    () => Text(
                  'Click on switch to change to ${_isLightTheme.value ? 'VN' : 'EN'}',
                ),
              ),
              ObxValue(
                    (data) => Switch(
                  value: _isLightTheme.value,
                  onChanged: (val) {
                    _isLightTheme.value = val;
                    Get.changeThemeMode(
                      _isLightTheme.value ? ThemeMode.light : ThemeMode.dark,
                    );
                    _saveThemeStatus();
                  },
                ),
                false.obs,
              ),
            ],
          ),
        ),
      ),
    );
  }
}