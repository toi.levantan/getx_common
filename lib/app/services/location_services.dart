// tanlv 20/02/23 create location_services
import 'dart:collection';
import 'dart:ui';
import 'package:get/get.dart';

import '../config/app_constants/language_path/en_lang.dart';
import '../config/app_constants/language_path/vn_lang.dart';


class LocalizationService extends Translations {

// locale sẽ được get mỗi khi mới mở app (phụ thuộc vào locale hệ thống hoặc bạn có thể cache lại locale mà người dùng đã setting và set nó ở đây)
  static final locale = _getLocaleFromLanguage();

// fallbackLocale là locale default nếu locale được set không nằm trong những Locale support
  static const fallbackLocale =  Locale('en', 'US');

// language code của những locale được support
  static final langCodes = [
    'vi',
    'en',
  ];

// các Locale được support
  static final locales = [
    const Locale('vi', 'VN'),
    const Locale('en', 'US'),
  ];


// cái này là Map các language được support đi kèm với mã code của lang đó: cái này dùng để đổ data vào Dropdownbutton và set language mà không cần quan tâm tới language của hệ thống
  static final langs = LinkedHashMap.from({
    'vi': 'Tiếng Việt',
    'en': 'English',
  });

// function change language nếu bạn không muốn phụ thuộc vào ngôn ngữ hệ thống
  static void changeLocale(String langCode) {
    final locale = _getLocaleFromLanguage(langCode: langCode);
    Get.updateLocale(locale!);
  }

  @override
  Map<String, Map<String, String>> get keys => {
    'vi_VN': vi,
    'en_US': en,
  };

  static Locale? _getLocaleFromLanguage({String? langCode}) {
    var lang = langCode ?? Get.deviceLocale!.languageCode;
    for (int i = 0; i < langCodes.length; i++) {
      if (lang == langCodes[i]) return locales[i];
    }
    return Get.locale;
  }
}

