import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../services/location_services.dart';

class Utils {
  // khai báo như này để không khởi tạo lại được
  Utils._();

  static String getImage({required String pathName}) {
    return 'assets/images/$pathName';
  }

  static Image getImageNetwork({required linkImage}) {
    return Image.network(linkImage);
  }
}

class Singleton {
  static Singleton? _instance;

  // không được khởi tạo lại
  Singleton._();

  static Singleton get instance => _instance ?? Singleton._();

  String? langValue(String langCode) {
    for (var element in LocalizationService.langCodes) {
      if (element == langCode) {
        return element;
      }
    }
    return null;
  }
}
class PreferenceUtils {
  static late SharedPreferences _preferenceUtils;

  // call this method from iniState() function of mainApp().
  static Future<SharedPreferences> get _instance => SharedPreferences.getInstance();
  static Future<SharedPreferences> init() async {
    _preferenceUtils = await _instance;
    return _preferenceUtils;
  }
  //

  static Future<bool> setString({required String key, required String value}) async {
    return _preferenceUtils.setString(key, value);
  }
  static Future<bool> setBool({required String key, required bool value}) async {
    return _preferenceUtils.setBool(key, value) ;
  }
  static String getString({required String key, String? defValue}) {
    return _preferenceUtils.getString(key) ?? defValue ?? "";
  }
  static bool getBool({required String key}) {
    return _preferenceUtils.getBool(key)?? false;
  }
  // Remove data for the 'counter' key.
  static Future<bool> remove({required String key}) async{
   return await _preferenceUtils.remove(key);
  }
  static Future<bool> clear() async => await _preferenceUtils.clear();
}


