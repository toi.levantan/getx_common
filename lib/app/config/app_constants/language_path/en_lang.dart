import 'package:getx_common/app/config/app_constants/language_path/language_path.dart';

const Map<String, String> en = {
   hello: 'Hello',
   multipleLanguage : 'Multiple language'
};
